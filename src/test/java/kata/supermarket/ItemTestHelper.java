package kata.supermarket;

import org.junit.jupiter.params.provider.Arguments;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ItemTestHelper {

    public static final String MILK_ID = "Milk";
    public static final String SWEETS_ID = "American Sweets";
    public static final String DIGESTIVES_ID = "Digestives";
    public static final String PICK_AND_MIX = "Pick And Mix";

    public static Arguments aSingleItemPricedByWeight() {
        return Arguments.of("a single weighed item", "1.25", "1.25",
                Collections.singletonList(twoFiftyGramsOfAmericanSweets()));
    }

    public static Arguments multipleDistinctItemsPricedByWeight() {
        return Arguments.of("multiple weighed items", "1.85", "1.85",
                Arrays.asList(twoFiftyGramsOfAmericanSweets(), twoHundredGramsOfPickAndMix())
        );
    }

    public static Arguments multipleDistinctItemsPricedPerUnit() {
        return Arguments.of("multiple items priced per unit", "2.04", "2.04",
                Arrays.asList(aPackOfDigestives(), aPintOfMilk()));
    }

    public static Arguments aSingleItemPricedPerUnit() {
        return Arguments.of("a single item priced per unit", "0.49", "0.49", Collections.singletonList(aPintOfMilk()));
    }

    public static Arguments noItems() {
        return Arguments.of("no items", "0.00", "0.00", Collections.emptyList());
    }

    public static List<Item> multipleItemsPricedByUnit() {
        return Arrays.asList(aPackOfDigestives(), aPintOfMilk(), aPackOfDigestives(), aPintOfMilk(),
                aPackOfDigestives(), aPintOfMilk(), aPackOfDigestives());
    }

    public static Item aPintOfMilk() {
        return new ProductByUnit(MILK_ID, new BigDecimal("0.49")).oneOf();
    }

    public static Item aPackOfDigestives() {
        return new ProductByUnit(DIGESTIVES_ID, new BigDecimal("1.55")).oneOf();
    }

    public static WeighedProduct aKiloOfAmericanSweets() {
        return new WeighedProduct(SWEETS_ID, new BigDecimal("4.99"));
    }

    public static Item twoFiftyGramsOfAmericanSweets() {
        return aKiloOfAmericanSweets().weighing(new BigDecimal(".25"));
    }

    public static WeighedProduct aKiloOfPickAndMix() {
        return new WeighedProduct(PICK_AND_MIX, new BigDecimal("2.99"));
    }

    public static Item twoHundredGramsOfPickAndMix() {
        return aKiloOfPickAndMix().weighing(new BigDecimal(".2"));
    }

    public static List<Item> multipleItemsPricedByWeights() {
        return Arrays.asList(
                aKiloOfAmericanSweets().weighing(new BigDecimal("1.2")),
                twoFiftyGramsOfAmericanSweets(),
                aKiloOfPickAndMix().weighing(BigDecimal.ONE),
                twoHundredGramsOfPickAndMix()
        );
    }

    public static Arguments multipleMixedItemsWithAppliedDiscounts() {
        return Arguments.of("multiple items with no matching discounts", "6.58", "3.54",
            Arrays.asList(
                    aPackOfDigestives(),
                    aPintOfMilk(),
                    aKiloOfPickAndMix().weighing(BigDecimal.ONE),
                    aPackOfDigestives()
            ));
    }

    private ItemTestHelper() {}
}
