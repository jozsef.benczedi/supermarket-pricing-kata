package kata.supermarket.discounts;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static kata.supermarket.discounts.DiscountTestHelper.twentyPercentOff;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PercentageDiscountTest {

    @Test
    public void testDiscountEffect() {
        BigDecimal initialPrice = new BigDecimal(100);
        BigDecimal expectedDiscount = new BigDecimal("20.00");
        DiscountEffect effect = twentyPercentOff();
        BigDecimal result = effect.getDiscountAmount(initialPrice);
        assertEquals(expectedDiscount, result);
    }
}
