package kata.supermarket.discounts;

import kata.supermarket.Product;
import kata.supermarket.ProductByUnit;

import java.math.BigDecimal;

public class DiscountTestHelper {

    public static final String DISCOUNT_ID = "Test Discount";
    private static final String DESCRIPTION = "A discount used for testing";

    public static DiscountEffect twentyPercentOff() {
        return xPercentOff(20);
    }

    private static DiscountEffect xPercentOff(int x) {
        return new PercentageDiscount(x);
    }

    public static DiscountEffect priceAtTenPounds() {
        return priceAtValue(BigDecimal.TEN);
    }

    private static DiscountEffect priceAtValue(BigDecimal value) {
        return new FixedPriceDiscount(value);
    }

    public static Discount twoForOne(Product product) {
        DiscountCondition condition = new DiscountCondition(2);
        DiscountEffect effect = priceAtValue(((ProductByUnit)product).pricePerUnit());
        return new Discount(DISCOUNT_ID, product.getId(), DESCRIPTION, condition, effect);
    }

    public static Discount kiloAtHalfPrice(Product product) {
        DiscountCondition condition = new DiscountCondition(BigDecimal.ONE);
        DiscountEffect effect = xPercentOff(50);
        return new Discount(DISCOUNT_ID, product.getId(), DESCRIPTION, condition, effect);
    }

    private DiscountTestHelper() {}
}
