package kata.supermarket.discounts;

import kata.supermarket.Item;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static kata.supermarket.ItemTestHelper.aKiloOfAmericanSweets;
import static kata.supermarket.ItemTestHelper.aKiloOfPickAndMix;
import static kata.supermarket.ItemTestHelper.aPackOfDigestives;
import static kata.supermarket.ItemTestHelper.aPintOfMilk;
import static kata.supermarket.ItemTestHelper.aSingleItemPricedByWeight;
import static kata.supermarket.ItemTestHelper.aSingleItemPricedPerUnit;
import static kata.supermarket.ItemTestHelper.multipleDistinctItemsPricedByWeight;
import static kata.supermarket.ItemTestHelper.multipleDistinctItemsPricedPerUnit;
import static kata.supermarket.ItemTestHelper.multipleItemsPricedByUnit;
import static kata.supermarket.ItemTestHelper.multipleItemsPricedByWeights;
import static kata.supermarket.ItemTestHelper.noItems;
import static kata.supermarket.discounts.DiscountTestHelper.kiloAtHalfPrice;
import static kata.supermarket.discounts.DiscountTestHelper.twoForOne;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DiscountManagerTest {

    private DiscountManager discountManager = new DiscountManager();

    @DisplayName("discount condition by unit amount not met when...")
    @MethodSource(value = "itemsByUnit")
    @ParameterizedTest(name = "{0}")
    public void twoForOneTestWithConditionsNotMet(String description,
                                                  String expectedSubTotal,
                                                  String expectedTotal,
                                                  List<Item> items) {
        //given
        items.forEach(item -> {
            Discount discount = twoForOne(item.getProduct());
            discountManager.addDiscount(discount);
        });
        //when
        List<AppliedDiscount> discounts = discountManager.applyDiscountsToItems(items);
        //then
        assertNotNull(discounts);
        assertTrue(discounts.isEmpty());
    }

    static Stream<Arguments> itemsByUnit() {
        return Stream.of(
                noItems(),
                aSingleItemPricedPerUnit(),
                multipleDistinctItemsPricedPerUnit()
        );
    }

    @Test
    public void twoForOneDiscountAppliedTest() {
        //given
        List<Item> items = multipleItemsPricedByUnit();
        discountManager.addDiscount(twoForOne(aPintOfMilk().getProduct()));
        discountManager.addDiscount(twoForOne(aPackOfDigestives().getProduct()));
        BigDecimal expectedTotalDiscount = new BigDecimal("3.59");
        //when
        List<AppliedDiscount> discounts = discountManager.applyDiscountsToItems(items);
        //then
        assertNotNull(discounts);
        assertFalse(discounts.isEmpty());
        BigDecimal sum = discounts.stream().map(AppliedDiscount::getDiscountAmount).reduce(BigDecimal::add).get();
        assertEquals(expectedTotalDiscount, sum);
    }

    @DisplayName("discount condition by item weight not met when...")
    @MethodSource(value = "itemsByWeight")
    @ParameterizedTest(name = "{0}")
    public void halfPriceTestWithConditionsNotMet(String description,
                                                  String expectedSubTotal,
                                                  String expectedTotal,
                                                  List<Item> items) {
        //given
        items.forEach(item -> {
            Discount discount = kiloAtHalfPrice(item.getProduct());
            discountManager.addDiscount(discount);
        });
        //when
        List<AppliedDiscount> discounts = discountManager.applyDiscountsToItems(items);
        //then
        assertNotNull(discounts);
        assertTrue(discounts.isEmpty());
    }

    static Stream<Arguments> itemsByWeight() {
        return Stream.of(
                noItems(),
                aSingleItemPricedByWeight(),
                multipleDistinctItemsPricedByWeight()
        );
    }

    @Test
    public void halfPriceDiscountAppliedTest() {
        //given
        List<Item> items = multipleItemsPricedByWeights();
        discountManager.addDiscount(kiloAtHalfPrice(aKiloOfAmericanSweets()));
        discountManager.addDiscount(kiloAtHalfPrice(aKiloOfPickAndMix()));
        BigDecimal expectedTotalDiscount = new BigDecimal("4.48");
        //when
        List<AppliedDiscount> discounts = discountManager.applyDiscountsToItems(items);
        //then
        assertNotNull(discounts);
        assertFalse(discounts.isEmpty());
        BigDecimal sum = discounts.stream().map(AppliedDiscount::getDiscountAmount).reduce(BigDecimal::add).get();
        assertEquals(expectedTotalDiscount, sum);
    }
}
