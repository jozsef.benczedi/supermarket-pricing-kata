package kata.supermarket.discounts;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static kata.supermarket.discounts.DiscountTestHelper.priceAtTenPounds;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FixedPriceDiscountTest {

    @Test
    public void testDiscountEffect() {
        BigDecimal initialPrice = new BigDecimal(100);
        BigDecimal expectedDiscount = new BigDecimal("90");
        DiscountEffect effect = priceAtTenPounds();
        BigDecimal result = effect.getDiscountAmount(initialPrice);
        assertEquals(expectedDiscount, result);
    }
}
