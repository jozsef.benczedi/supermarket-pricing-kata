package kata.supermarket;

import kata.supermarket.discounts.DiscountManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static kata.supermarket.ItemTestHelper.aKiloOfAmericanSweets;
import static kata.supermarket.ItemTestHelper.aKiloOfPickAndMix;
import static kata.supermarket.ItemTestHelper.aPackOfDigestives;
import static kata.supermarket.ItemTestHelper.aPintOfMilk;
import static kata.supermarket.ItemTestHelper.aSingleItemPricedByWeight;
import static kata.supermarket.ItemTestHelper.aSingleItemPricedPerUnit;
import static kata.supermarket.ItemTestHelper.multipleDistinctItemsPricedByWeight;
import static kata.supermarket.ItemTestHelper.multipleDistinctItemsPricedPerUnit;
import static kata.supermarket.ItemTestHelper.multipleMixedItemsWithAppliedDiscounts;
import static kata.supermarket.ItemTestHelper.noItems;
import static kata.supermarket.discounts.DiscountTestHelper.kiloAtHalfPrice;
import static kata.supermarket.discounts.DiscountTestHelper.twoForOne;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BasketTest {

    private final DiscountManager discountManager = new DiscountManager();

    @BeforeEach
    public void setup() {
        discountManager.addDiscount(kiloAtHalfPrice(aKiloOfPickAndMix()));
        discountManager.addDiscount(kiloAtHalfPrice(aKiloOfAmericanSweets()));
        discountManager.addDiscount(twoForOne(aPintOfMilk().getProduct()));
        discountManager.addDiscount(twoForOne(aPackOfDigestives().getProduct()));
    }

    @DisplayName("basket provides its total value when containing...")
    @MethodSource(value = "basketProvidesTotalValue")
    @ParameterizedTest(name = "{0}")
    void basketProvidesTotalValue(String description,
                                  String expectedSubTotal,
                                  String expectedTotal,
                                  Iterable<Item> items) {
        final Basket basket = new Basket(discountManager);
        items.forEach(basket::add);
        assertEquals(new BigDecimal(expectedSubTotal), basket.subTotal());
        assertEquals(new BigDecimal(expectedTotal), basket.total());
    }

    static Stream<Arguments> basketProvidesTotalValue() {
        return Stream.of(
                noItems(),
                aSingleItemPricedPerUnit(),
                multipleDistinctItemsPricedPerUnit(),
                aSingleItemPricedByWeight(),
                multipleDistinctItemsPricedByWeight(),
                multipleMixedItemsWithAppliedDiscounts()
        );
    }
}
