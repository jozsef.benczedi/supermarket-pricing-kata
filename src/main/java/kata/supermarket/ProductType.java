package kata.supermarket;

public enum ProductType {
    BY_UNIT,
    BY_WEIGHT
}
