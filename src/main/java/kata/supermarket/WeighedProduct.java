package kata.supermarket;

import java.math.BigDecimal;

import static kata.supermarket.ProductType.BY_WEIGHT;

public class WeighedProduct implements Product {

    private String id;
    private final BigDecimal pricePerKilo;

    public WeighedProduct(String id, final BigDecimal pricePerKilo) {
        this.id = id;
        this.pricePerKilo = pricePerKilo;
    }

    public String getId() {
        return id;
    }

    BigDecimal pricePerKilo() {
        return pricePerKilo;
    }

    public ProductType getType() {
        return BY_WEIGHT;
    }

    public Item weighing(final BigDecimal kilos) {
        return new ItemByWeight(this, kilos);
    }
}
