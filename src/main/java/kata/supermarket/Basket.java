package kata.supermarket;

import kata.supermarket.discounts.AppliedDiscount;
import kata.supermarket.discounts.DiscountManager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Basket {
    private final DiscountManager discountManager;
    private final List<Item> items;

    public Basket(DiscountManager discountManager) {
        this.items = new ArrayList<>();
        this.discountManager = discountManager;
    }
    public void add(final Item item) {
        this.items.add(item);
    }

    List<Item> items() {
        return Collections.unmodifiableList(items);
    }

    public BigDecimal subTotal() {
        return new TotalCalculator().subtotal();
    }

    public BigDecimal total() {
        return new TotalCalculator().calculate();
    }

    private class TotalCalculator {
        private final List<Item> items;
        private List<AppliedDiscount> discounts;

        TotalCalculator() {
            this.items = items();
            this.discounts = discountManager.applyDiscountsToItems(items);
        }

        private BigDecimal subtotal() {
            return items.stream().map(Item::price)
                    .reduce(BigDecimal::add)
                    .orElse(BigDecimal.ZERO)
                    .setScale(2, RoundingMode.HALF_UP);
        }

        private BigDecimal discounts() {
            return discounts.stream().map(AppliedDiscount::getDiscountAmount)
                    .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        }

        private BigDecimal calculate() {
            return subtotal().subtract(discounts());
        }
    }
}
