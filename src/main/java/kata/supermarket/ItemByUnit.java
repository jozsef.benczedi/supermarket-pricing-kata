package kata.supermarket;

import java.math.BigDecimal;

public class ItemByUnit implements Item {

    private final ProductByUnit product;
    private BigDecimal numberOfUnits;

    ItemByUnit(final ProductByUnit product) {
        this.product = product;
        this.numberOfUnits = BigDecimal.ONE;
    }

    public ProductByUnit getProduct() {
        return product;
    }

    public BigDecimal price() {
        return numberOfUnits.multiply(product.pricePerUnit());
    }

    public BigDecimal getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(BigDecimal numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }
}
