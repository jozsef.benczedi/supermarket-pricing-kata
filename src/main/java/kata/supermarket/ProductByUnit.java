package kata.supermarket;

import java.math.BigDecimal;

import static kata.supermarket.ProductType.BY_UNIT;

public class ProductByUnit implements Product {

    private String id;
    private final BigDecimal pricePerUnit;

    public ProductByUnit(String id, final BigDecimal pricePerUnit) {
        this.id = id;
        this.pricePerUnit = pricePerUnit;
    }

    public String getId() {
        return id;
    }

    public BigDecimal pricePerUnit() {
        return pricePerUnit;
    }

    public ProductType getType() {
        return BY_UNIT;
    }

    public Item oneOf() {
        return new ItemByUnit(this);
    }
}
