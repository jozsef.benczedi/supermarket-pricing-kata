package kata.supermarket;

public interface Product {

    String getId();

    ProductType getType();
}
