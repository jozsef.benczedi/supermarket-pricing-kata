package kata.supermarket;

import java.math.BigDecimal;

public interface Item {

    Product getProduct();

    BigDecimal price();
}
