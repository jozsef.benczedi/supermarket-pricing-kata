package kata.supermarket.discounts;

import java.math.BigDecimal;

public class DiscountCondition {
    private final int amountPurchased;
    private final BigDecimal weightPurchased;

    public DiscountCondition(int amountPurchased) {
        this.amountPurchased = amountPurchased;
        this.weightPurchased = BigDecimal.ZERO;
    }

    public DiscountCondition(BigDecimal weightPurchased) {
        this.amountPurchased = 0;
        this.weightPurchased = weightPurchased;
    }

    public int getAmountPurchased() {
        return amountPurchased;
    }

    public BigDecimal getWeightPurchased() {
        return weightPurchased;
    }
}
