package kata.supermarket.discounts;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PercentageDiscount implements DiscountEffect {

    private BigDecimal discountPercentage;

    public PercentageDiscount(int discountPercentage) {
        this.discountPercentage = new BigDecimal(discountPercentage)
                .setScale(2, RoundingMode.HALF_DOWN)
                .divide(new BigDecimal(100), RoundingMode.HALF_DOWN);
    }

    public BigDecimal getDiscountAmount(BigDecimal initialPrice) {
        return initialPrice.multiply(discountPercentage).setScale(2, RoundingMode.HALF_DOWN);
    }
}
