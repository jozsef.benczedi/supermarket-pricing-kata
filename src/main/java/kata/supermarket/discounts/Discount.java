package kata.supermarket.discounts;

public class Discount {
    private final String id;
    private final String productId;
    private final String description;
    private final DiscountCondition precondition;
    private final DiscountEffect effect;
    private boolean active;

    public Discount(String id, String productId, String description,
                    DiscountCondition precondition, DiscountEffect effect) {
        this.id = id;
        this.productId = productId;
        this.description = description;
        this.precondition = precondition;
        this.effect = effect;
        this.active = true;
    }

    public String getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public String getDescription() {
        return description;
    }

    public DiscountCondition getPrecondition() {
        return precondition;
    }

    public DiscountEffect getEffect() {
        return effect;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
