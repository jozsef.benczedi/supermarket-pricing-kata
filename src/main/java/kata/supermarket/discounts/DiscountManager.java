package kata.supermarket.discounts;

import kata.supermarket.Item;
import kata.supermarket.ItemByWeight;
import kata.supermarket.ProductType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DiscountManager {

    private final List<Discount> discounts;

    public DiscountManager() {
        this.discounts = new ArrayList<>();
    }

    public void addDiscount(Discount discount) {
        discounts.add(discount);
    }

    public List<AppliedDiscount> applyDiscountsToItems(List<Item> items) {
        List<AppliedDiscount> result = new ArrayList<>();
        discounts.stream()
                .filter(Discount::isActive)
                .map(discount -> mapDiscountToItems(discount, items))
                .forEach(result::addAll);

        return result;
    }

    private List<AppliedDiscount> mapDiscountToItems(Discount discount, List<Item> items) {
        List<AppliedDiscount> result = new ArrayList<>();
        List<Item> matchedItems = items.stream()
                .filter(item -> item.getProduct().getId().equals(discount.getProductId()))
                .collect(Collectors.toList());
        if (matchedItems.isEmpty()) return result;
        ProductType productType = matchedItems.get(0).getProduct().getType();
        switch (productType) {
            case BY_UNIT: return applyDiscountsToItemsByUnit(discount, matchedItems);
            case BY_WEIGHT: return applyDiscountsToItemsByWeight(discount, matchedItems);
            default: break;
        }
        return result;
    }

    private List<AppliedDiscount> applyDiscountsToItemsByWeight(Discount discount, List<Item> matchedItems) {
        List<AppliedDiscount> result = new ArrayList<>();
        matchedItems.forEach(item -> {
            ItemByWeight weightedItem = (ItemByWeight) item;
            if (weightedItem.getWeightInKilos().compareTo(discount.getPrecondition().getWeightPurchased()) >= 0) {
                AppliedDiscount applied = new AppliedDiscount(discount.getId(), discount.getDescription(),
                        discount.getEffect().getDiscountAmount(weightedItem.price()));
                result.add(applied);
            }
        });

        return result;
    }

    private List<AppliedDiscount> applyDiscountsToItemsByUnit(Discount discount, List<Item> matchedItems) {
        List<AppliedDiscount> result = new ArrayList<>();
        int count = 0;
        BigDecimal sum = BigDecimal.ZERO;
        for (Item matchedItem : matchedItems) {
            count++;
            sum = sum.add(matchedItem.price());

            if (count == discount.getPrecondition().getAmountPurchased()) {
                AppliedDiscount applied = new AppliedDiscount(discount.getId(), discount.getDescription(),
                        discount.getEffect().getDiscountAmount(sum));
                count = 0;
                sum = BigDecimal.ZERO;
                result.add(applied);
            }
        }

        return result;
    }
}
