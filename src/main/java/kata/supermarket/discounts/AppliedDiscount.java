package kata.supermarket.discounts;

import java.math.BigDecimal;

public class AppliedDiscount {
    private final String discountId;
    private final String description;
    private final BigDecimal discountAmount;

    public AppliedDiscount(String discountId, String description, BigDecimal discountAmount) {
        this.discountId = discountId;
        this.description = description;
        this.discountAmount = discountAmount;
    }

    public String getDiscountId() {
        return discountId;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }
}
