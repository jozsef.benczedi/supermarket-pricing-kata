package kata.supermarket.discounts;

import java.math.BigDecimal;

public class FixedPriceDiscount implements DiscountEffect {

    private BigDecimal price;

    public FixedPriceDiscount(BigDecimal price) {
        this.price = price;
    }

    @Override
    public BigDecimal getDiscountAmount(BigDecimal initialPrice) {
        //the assumption here is that the (discount) price will be lower than the price of the items
        return initialPrice.subtract(price);
    }
}
