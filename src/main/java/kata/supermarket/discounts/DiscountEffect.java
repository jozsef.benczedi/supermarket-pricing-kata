package kata.supermarket.discounts;

import java.math.BigDecimal;

public interface DiscountEffect {

    BigDecimal getDiscountAmount(BigDecimal initialPrice);

}
