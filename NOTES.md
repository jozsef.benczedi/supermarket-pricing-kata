# Notes

The way I imagine this is that each discount a supermarket offers will have a set of conditions, and when the conditions are met a set of effects will take place.
For the effects I implemented two basic ones from the examples, for the conditions I kept it even simpler.
Ideally the conditions would also implement an interface, and we should be able to construct more complex conditions.

I refactored the products to always have an id, and the discounts will be linked to a product by the product's id.
All the discounts happen in the discount manager, in the future I will want to refactor it as well to implement an interface, where I just apply the discounts to the items in the basket, and different classes will handle the logic.
